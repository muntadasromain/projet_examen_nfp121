
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SaisiesSwing {

	public String nomFichier;
	public List<Double> valeurs = new ArrayList<Double>();
	public List<Position> positions = new ArrayList<Position>();

	JFrame frame = new JFrame();

	public SaisiesSwing(String nom) {
		this.nomFichier = nom;

		frame.setTitle("Saisie données");
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 150);

		JLabel labelX = new JLabel("Abscisse");
		JLabel labelY = new JLabel("Ordonnee");
		JLabel labelValeur = new JLabel("Valeur");
		labelX.setHorizontalAlignment(JLabel.CENTER);
		labelY.setHorizontalAlignment(JLabel.CENTER);
		labelValeur.setHorizontalAlignment(JLabel.CENTER);

		JTextField textFieldX = new JTextField();
		JTextField textFieldY = new JTextField();
		JTextField textFieldValeur = new JTextField();

		JButton valider = new JButton("Valider");
		JButton effacer = new JButton("Effacer");
		JButton terminer = new JButton("Terminer");

		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(3, 3));

		panel.add(labelX);
		panel.add(labelY);
		panel.add(labelValeur);

		panel.add(textFieldX);
		panel.add(textFieldY);
		panel.add(textFieldValeur);

		panel.add(valider);
		panel.add(effacer);
		panel.add(terminer);

		frame.setContentPane(panel);

		valider.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!(isInteger(textFieldX.getText()))) {
					textFieldX.setBackground(Color.RED);
				}
				if (!(isInteger(textFieldY.getText()))) {
					textFieldY.setBackground(Color.RED);
				}
				if (!(isDouble(textFieldValeur.getText()))) {
					textFieldValeur.setBackground(Color.RED);
				}
				if (isDouble(textFieldValeur.getText()) && isInteger(textFieldY.getText())
						&& isInteger(textFieldX.getText())) {
					valeurs.add(Double.parseDouble(textFieldValeur.getText()));
					positions.add(new Position(Integer.parseInt(textFieldX.getText()),
							Integer.parseInt(textFieldY.getText())));
					textFieldX.setText("");
					textFieldX.setBackground(Color.WHITE);
					textFieldY.setText("");
					textFieldY.setBackground(Color.WHITE);
					textFieldValeur.setText("");
					textFieldValeur.setBackground(Color.WHITE);
				}
			}
		});

		effacer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				textFieldX.setText("");
				textFieldY.setText("");
				textFieldValeur.setText("");
			}
		});

		terminer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (positions.size() == 0 || valeurs.size() == 0) {
					textFieldX.setBackground(Color.RED);
					textFieldY.setBackground(Color.RED);
					textFieldValeur.setBackground(Color.RED);
				} else {
					if (!(textFieldX.getText().isEmpty()) && !(textFieldY.getText().isEmpty()) && !(textFieldValeur.getText().isEmpty())) {
						valeurs.add(Double.parseDouble(textFieldValeur.getText()));
						positions.add(new Position(Integer.parseInt(textFieldX.getText()),
								Integer.parseInt(textFieldY.getText())));
					}
					terminerSasie();
					frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
				}
			}
		});

		frame.setVisible(true);
	}

	public boolean isDouble(String s) {
		if (s == null) {
			return false;
		}
		try {
			double d = Double.parseDouble(s);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	public boolean isInteger(String s) {
		if (s == null) {
			return false;
		}
		try {
			int i = Integer.parseInt(s);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	public void terminerSasie() {
		String data = "";
		for (int i = 0; i < this.valeurs.size(); i++) {
			data = data + positions.get(i).x + " " + positions.get(i).y + " " + (i + 1) + " " + valeurs.get(i)
					+ System.getProperty("line.separator");
		}
		this.genererFichier(data);
	}

	public void genererFichier(String data) {
		OutputStream os = null;
		try {
			os = new FileOutputStream(new File(this.nomFichier + ".txt"));
			os.write(data.getBytes(), 0, data.length());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}

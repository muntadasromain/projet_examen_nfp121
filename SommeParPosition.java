import java.util.*;

/**
 * SommeParPosition
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */

public class SommeParPosition extends Traitement {

	private HashMap<String, Double> donnees = new HashMap<String, Double>();

	public String sommeParPosition() {
		String s = "";
		for (Map.Entry mapentry : donnees.entrySet()) {
			s += "	  - Position" + mapentry.getKey() + " -> " + mapentry.getValue() + "\n";
		}
		return s;
	}

	public void traiter(Position position, double valeur) {
		donnees.merge("(" + position.x + ", " + position.y + ")", valeur, Double::sum);
		super.traiter(position, valeur);
	}

}

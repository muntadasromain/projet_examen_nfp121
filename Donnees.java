import java.util.ArrayList;
import java.util.List;

/**
  * Donnees enregistre toutes les données reçues, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Donnees extends Traitement {

	private List<Donnee> donnees = new ArrayList<Donnee>();
	
	public List<Donnee> donnees() {
		return this.donnees;
	}
	
	public void traiter(Position position, double valeur) {
		this.donnees.add(new Donnee(position, valeur));
		super.traiter(position, valeur);
	}

}

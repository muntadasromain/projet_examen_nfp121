
public class Donnee {

	Position position;
	double valeur;
	
	public Donnee(Position position, double valeur) {
		this.position = position;
		this.valeur = valeur;
	}
	
	public Position getPosition() {
		return this.position;
	}
	
	public double getValeur() {
		return this.valeur;
	}
	
	public String toString() {
		return "Position : " + getPosition().toString() + ", valeur : " + getValeur();
	}
	
}

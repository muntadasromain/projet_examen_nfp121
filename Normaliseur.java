import java.util.ArrayList;
import java.util.List;

/**
  * Normaliseur normalise les données d'un lot en utilisant une transformation affine.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Normaliseur extends Traitement {

	private Donnees donnees;
	private Max max;
	private Max max2;
	private double debut, fin;
	private List<Donnee> listDonnees = new ArrayList<Donnee>();
	
	public Normaliseur(double debut, double fin) {
		this.debut = debut;
		this.fin = fin;
	}
	
	@Override
	public void gererDebutLotLocal(String nomLot) {
		FabriqueTraitement traitements = new FabriqueTraitementConcrete();
		donnees = traitements.donnees();
		max = traitements.max();
		Multiplicateur multiplicateur = traitements.multiplicateur(-1);
		max2 = traitements.max();
		
		max.ajouterSuivants(multiplicateur);
		multiplicateur.ajouterSuivants(max2);
		System.out.println("test");
	}
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		this.listDonnees = this.donnees.donnees();
		
		for(Donnee d : this.listDonnees) {
			System.out.println(d.position + " : " + ((this.max.max()- (-this.max2.max()))/(this.fin - this.debut)) * d.valeur + (this.debut - ((this.max.max()-(-this.max2.max())/(this.fin - this.debut)) * (-this.max2.max()))));
		}
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		donnees.traiter(position, valeur);
		//super.traiter(position, ((this.vMax-this.vMin)/(this.fin - this.debut)) * valeur + (this.debut - ((this.vMax-this.vMin)/(this.fin - this.debut)) * this.vMin));
	}

}
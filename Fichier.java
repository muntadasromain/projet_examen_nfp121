import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.filter.ElementFilter;
import org.jdom2.input.SAXBuilder;


public class Fichier {

	private String path;
	private String type;
	private List<SimpleImmutableEntry<Position, Double>> donnees = new ArrayList<SimpleImmutableEntry<Position, Double>>();

	public Fichier(String path) {
		this.path = path;
		definirType();
		lire();
	}

	public void definirType() {
		try (BufferedReader in = new BufferedReader(new FileReader(path))) {
			String ligne = in.readLine();
			String[] mots = ligne.split("\\s+");
			if (mots.length == 4) {
				this.type = "1";
			} else if (mots.length == 6) {
				this.type = "2";
			} else {
				this.type = "xml";
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void lire() {
		if (type == "1") {
			try (BufferedReader in = new BufferedReader(new FileReader(path))) {
				String ligne = null;
				while ((ligne = in.readLine()) != null) {
					String[] mots = ligne.split("\\s+");
					assert mots.length == 4;
					double valeur = Double.parseDouble(mots[3]);
					int x = Integer.parseInt(mots[0]);
					int y = Integer.parseInt(mots[1]);
					Position p = new Position(x, y);
					this.donnees.add(new SimpleImmutableEntry<Position, Double>(p, valeur));
				}
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		} else if (type == "2") {
			try (BufferedReader in = new BufferedReader(new FileReader(path))) {
				String ligne = null;
				while ((ligne = in.readLine()) != null) {
					String[] mots = ligne.split("\\s+");
					assert mots.length == 6;
					double valeur = Double.parseDouble(mots[4]);
					int x = Integer.parseInt(mots[1]);
					int y = Integer.parseInt(mots[2]);
					Position p = new Position(x, y);
					this.donnees.add(new SimpleImmutableEntry<Position, Double>(p, valeur));
				}
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		} else if (type == "xml") {
			SAXBuilder sxb = new SAXBuilder();
			try {
				Document doc = sxb.build(new File(path));
				
				for(Element e : doc.getDescendants(new ElementFilter("donnee"))) {
					double valeur = Double.parseDouble(e.getChildText("valeur"));
					int x = Integer.parseInt(e.getAttributeValue("x"));
					int y = Integer.parseInt(e.getChildText("y"));
					Position p = new Position(x, y);
					this.donnees.add(new SimpleImmutableEntry<Position, Double>(p, valeur));
				}
				
			} catch (JDOMException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public Iterable<SimpleImmutableEntry<Position, Double>> getDonnees() {
		return donnees;
	}

}

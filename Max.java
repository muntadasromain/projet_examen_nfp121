import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
  * Max calcule le max des valeurs vues, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class Max extends Traitement {

	private List<Double> valeurs = new ArrayList<Double>();
	
	public double max() {
		return Collections.max(valeurs);
	}
	
	public void traiter(Position position, double valeur) {
		this.valeurs.add(valeur);
		super.traiter(position, valeur);
	}

}

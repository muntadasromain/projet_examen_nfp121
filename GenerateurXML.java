import java.io.FileWriter;

import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 * GenerateurXML écrit dans un fichier, à charque fin de lot, toutes les données
 * lues en indiquant le lot dans le fichier XML.
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class GenerateurXML extends Traitement {

	private String nomFichier;
	private int id = 0;
	private Element racine = new Element("donnees");
	
	public GenerateurXML(String nomFichier) {
		this.nomFichier = nomFichier;
	}
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		try {
            FileWriter writer = new FileWriter(this.nomFichier+".xml");
            XMLOutputter outputter = new XMLOutputter();
            outputter.setFormat(Format.getPrettyFormat());
            outputter.output(racine, writer);
            System.out.println("Données enregistrées dans le fichier "+this.nomFichier + ".xml à la racine du projet");
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

	public void traiter(Position position, double valeur) {
		this.racine.addContent(addDonnee(position, valeur));
		super.traiter(position, valeur);
	}
	
	public Element addDonnee(Position position, double valeur) {
		Element donnee = new Element("donnee");
		Element y = new Element("y");
		Element valeurElt = new Element("valeur");
		
		donnee.setAttribute("x", position.x+"");
		donnee.setAttribute("id", this.id+"");
		
		y.setText(position.y+"");
		valeurElt.setText(valeur+"");
		
		donnee.addContent(y);
		donnee.addContent(valeurElt);
		
		this.id++;
		
		return donnee;
	}
}

import java.util.*;

/**
  * Positions enregistre toutes les positions, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class Positions extends PositionsAbstrait {

	private List<Position> positions = new ArrayList<Position>();
	
	@Override
	public int nombre() {
		return positions.size();
	}

	@Override
	public Position position(int indice) {
		return positions.get(indice);
	}

	@Override
	public int frequence(Position position) {
		int frequence = 0;
		for(Position p : positions) {
			if(p.equals(position))
				frequence++;
		}
		return frequence;
	}

	public void traiter(Position position, double valeur) {
		this.positions.add(position);
		super.traiter(position, valeur);
	}

}

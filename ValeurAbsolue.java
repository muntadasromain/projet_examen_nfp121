
public class ValeurAbsolue extends Traitement {

	@Override
	public void traiter(Position position, double valeur) {
		if(valeur < 0) {
			valeur = -valeur;
		}
		super.traiter(position, valeur);
	}
	
}

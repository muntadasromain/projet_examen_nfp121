import java.io.FileNotFoundException;
import java.util.Scanner;

public class MenuPrincipal {

	static String rep = "";

	public static void main(String[] args) {
		lancerMenu();
	}

	public static void lancerMenu() {
		do {
			System.out.println("Menu Principal");
			System.out.println("1 - executer l'exemple 1");
			System.out.println("2 - executer l'exemple 2 (non fonctionnel)");
			System.out.println("3 - Saisir des données");
			System.out.println("4 - Lire une source");
			System.out.print("\nVeuillez saisir le numéro correspondant à l'action de votre choix : ");

			Scanner sc = new Scanner(System.in);
			rep = sc.nextLine();

			if (rep.charAt(0) == '1') {
				exemple1();
			} else if (rep.charAt(0) == '2') {

			} else if (rep.charAt(0) == '3') {
				saisieDonnees();
			} else if (rep.charAt(0) == '4') {
				lectureSource();
			} else {
				rep = "invalide";
				System.out.println("Saisie invalide, veuillez recommencer.");
			}
		} while (rep.equals("invalide"));
	}

	public static void saisieDonnees() {
		System.out.println("\nVeuillez saisir le nom du fichier : ");
		Scanner sc = new Scanner(System.in);
		String nomFichier = sc.nextLine();
		new SaisiesSwing(nomFichier);

		System.out.println("\nExemple de traitement : génération du fichier au format xml");

		Fichier fichier = new Fichier(nomFichier + ".txt");

		FabriqueTraitement traitements = new FabriqueTraitementConcrete();

		GenerateurXML generateurXML = traitements.generateurXML("exemple saisie de données");
		Analyseur analyseur = new Analyseur(generateurXML);
		analyseur.traiter(fichier.getDonnees(), "Exemple lecture source");
	}

	public static void lectureSource() {
		System.out.println("\nVeuillez saisir le chemin d'accès du fichier : ");
		Scanner sc = new Scanner(System.in);
		String chemin = sc.nextLine();
		Fichier fichier = new Fichier(chemin);

		System.out.println("\nExemple de traitement");

		FabriqueTraitement traitements = new FabriqueTraitementConcrete();

		SommeAbstrait somme = traitements.somme();
		PositionsAbstrait positions = traitements.positions();
		Multiplicateur multiplicateur = traitements.multiplicateur(10);
		SommeAbstrait somme2 = traitements.somme();

		somme.ajouterSuivants(positions);
		positions.ajouterSuivants(multiplicateur);
		multiplicateur.ajouterSuivants(somme2);

		Analyseur analyseur = new Analyseur(somme);
		analyseur.traiter(fichier.getDonnees(), "Exemple lecture source");

		System.out.println("Traitements : " + somme);
		System.out.println("Somme : " + somme.somme());
		System.out.println("Nombre de positions : " + positions.nombre());
		System.out.println("Multiplication par 10");
		System.out.println("Somme2 : " + somme2.somme());
	}

	public static void exemple1() {
		System.out.println();
		System.out.println("=== exemple1() ===");

		FabriqueTraitement traitements = new FabriqueTraitementConcrete();

		// Construire le traitement
		SommeAbstrait somme = traitements.somme();
		PositionsAbstrait positions = traitements.positions();
		somme.ajouterSuivants(positions);

		System.out.println("Traitement : " + somme);

		// Traiter des donnÃ©es manuelles
		somme.gererDebutLot("manuelles");
		somme.traiter(new Position(1, 1), 5.0);
		somme.traiter(new Position(1, 2), 2.0);
		somme.traiter(new Position(1, 1), -1.0);
		somme.traiter(new Position(1, 2), 1.5);
		somme.gererFinLot("manuelles");

		// Exploiter les rÃ©sultats
		System.out.println("Somme = " + somme.somme());
		System.out.println("Positions.frequence(new Position(1,2)) = " + positions.frequence(new Position(1, 2)));
	}

	public static void exemple2(String traitements) throws FileNotFoundException {
		System.out.println();
		System.out.println("=== exemple2(" + traitements + ") ===");

		// Construire les traitements
		TraitementBuilder builder = new TraitementBuilder();
		Traitement main = builder.traitement(new java.util.Scanner(traitements), null);

		System.out.println("Traitement : " + main);

		// Traiter des données manuelles
		main.gererDebutLot("manuelles");
		main.traiter(new Position(1, 1), 5.0);
		main.traiter(new Position(1, 2), 2.0);
		main.traiter(new Position(1, 1), -1.0);
		main.gererFinLot("manuelles");

		// Construire l'analyseur
		Analyseur analyseur = new Analyseur(main);

		// Traiter les autres sources de données : "donnees.txt", etc.
	}

}
